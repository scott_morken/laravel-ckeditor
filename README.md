## Laravel 5 CKEditor package

### License

This software is open-sourced software licensed under the [MIT license](http://opensource.org/licenses/MIT)

The Laravel framework is open-sourced software licensed under the [MIT license](http://opensource.org/licenses/MIT)

CKEditor is licensed under the [Mozilla Public License Version 1.1](http://www.mozilla.org/MPL/MPL-1.1.html)

### Requirements
* PHP 5.5+
* [Composer](https://getcomposer.org/)

#### Use

* add service provider to `config/app.php`

```
'providers' => [
...
  Smorken\Ckeditor\ServiceProvider::class,
```

* add facade to `config/app.php`

```
'aliases' => [
...
  'Ckeditor'  => Smorken\Ckeditor\Facades\Ckeditor::class,
```

* add middleware to `app/Http/Kernel.php`

```
protected $middleware = [
...
  \Smorken\Ckeditor\Middleware\Ckeditor::class,
```

* add assets (gulp example)

gulp.src([
        './vendor/ckeditor/ckeditor/**/*.{css,js,png,gif}',
        '!./vendor/ckeditor/ckeditor/samples',
        '!./vendor/ckeditor/ckeditor/samples/**'
    ])
        .pipe(gulp.dest('./public/js/ckeditor'));
```


Creating an editor instance:

```
//index.blade.php
{!! Ckeditor::create('name') !!}
```

`Ckeditor::create` signature:

`Ckeditor::create($name, $value = null, $text_area_options = [], $ckeditor_options = [])`
