<script type="text/javascript" id="ckeditor-{{ $id }}">
    var ckeditoredit{{ camel_case($id) }} = CKEDITOR.replace('{{ $id }}'{!! $options ? ', ' . json_encode($options) : null !!});
</script>
