<?php
/**
 * Created by IntelliJ IDEA.
 * User: smorken
 * Date: 4/4/14
 * Time: 6:14 PM
 */
/* @var \Illuminate\Support\Collection $options
 * @var string $source
 * @var array $registered
 */
?>
<script type="text/javascript" src="{{ $source }}"></script>
@foreach($registered as $id)
    @include('smorken/ckeditor::jsline', ['id' => $id, 'options' => $options->get($id, null)])
@endforeach
