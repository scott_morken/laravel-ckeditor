<?php
/**
 * Created by IntelliJ IDEA.
 * User: scoce95461
 * Date: 6/20/16
 * Time: 11:07 AM
 */

namespace Smorken\Ckeditor\Contracts;

interface Ckeditor
{

    public function create($name, $value = null, array $options = [], array $editor_opts = []);

    public function generateId($name);

    public function registered();

    public function register($id);

    public function addEditorOptions($id, array $editor_options = []);

    /**
     * @param $request
     * @param \Symfony\Component\HttpFoundation\Response $response
     * Borrowed from Barryvdh/LaravelDebugBar
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function modifyResponse($request, $response);

    public function generateJavascript();

    public function isRegistered();

    public function options();

    public function source();
}
