<?php
/**
 * Created by IntelliJ IDEA.
 * User: smorken
 * Date: 4/4/14
 * Time: 6:25 PM
 */

namespace Smorken\Ckeditor\Facades;

class Ckeditor extends \Illuminate\Support\Facades\Facade
{

    protected static function getFacadeAccessor()
    {
        return \Smorken\Ckeditor\Contracts\Ckeditor::class;
    }
}
