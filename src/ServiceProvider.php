<?php namespace Smorken\Ckeditor;

class ServiceProvider extends \Illuminate\Support\ServiceProvider
{

    protected $defer = false;

    /**
     * Bootstrap the application events.
     *
     * @return void
     */
    public function boot()
    {
        $this->loadViewsFrom(__DIR__ . '/../views', 'smorken/ckeditor');
        $this->publishes(
            [
                __DIR__ . '/../views' => base_path('resources/views/vendor/smorken/ckeditor'),
            ],
            'views'
        );
        $this->publishes(
            [
                __DIR__ . '/../config/config.php' => config_path('ckeditor.php'),
            ],
            'config'
        );
        $this->registerMiddleware('Smorken\Ckeditor\Middleware\Ckeditor');
    }

    /**
     * Register the service provider.
     *
     * @return void
     */
    public function register()
    {
        $this->registerResources();
        $this->app->singleton(
            'Smorken\Ckeditor\Contracts\Ckeditor',
            function ($app) {
                $config = $app['config']->get('ckeditor', []);
                return new Ckeditor($app['view'], $app['form'], $app['url'], $config);
            }
        );
    }

    protected function registerResources()
    {
        $this->mergeConfigFrom(__DIR__ . '/../config/config.php', 'ckeditor');
    }

    protected function registerMiddleware($middleware)
    {
        $kernel = $this->app['Illuminate\Contracts\Http\Kernel'];
        $kernel->pushMiddleware($middleware);
    }

    public function provides()
    {
        return ['Smorken\Ckeditor\Contracts\Ckeditor'];
    }
}
