<?php
/**
 * Created by IntelliJ IDEA.
 * User: scoce95461
 * Date: 6/20/16
 * Time: 10:41 AM
 */

namespace Smorken\Ckeditor\Middleware;

use Closure;
use Exception;
use Illuminate\Contracts\Container\Container;
use Illuminate\Contracts\Debug\ExceptionHandler;
use Illuminate\Http\Request;

class Ckeditor
{

    /**
     * @var \Smorken\Ckeditor\Ckeditor
     */
    protected $ckeditor;

    /**
     * @var Container
     */
    protected $container;

    public function __construct(Container $container, \Smorken\Ckeditor\Contracts\Ckeditor $ckeditor)
    {
        $this->container = $container;
        $this->ckeditor = $ckeditor;
    }

    public function handle($request, Closure $next)
    {
        try {
            /** @var \Illuminate\Http\Response $response */
            $response = $next($request);
        } catch (Exception $e) {
            $response = $this->handleException($request, $e);
        }
        $this->ckeditor->modifyResponse($request, $response);
        return $response;
    }

    /**
     * Handle the given exception.
     *
     * (Copy from Illuminate\Routing\Pipeline by Taylor Otwell)
     *
     * @param $passable
     * @param  Exception $e
     * @return mixed
     * @throws Exception
     */
    protected function handleException($passable, Exception $e)
    {
        if (!$this->container->bound(ExceptionHandler::class) || !$passable instanceof Request) {
            throw $e;
        }

        $handler = $this->container->make(ExceptionHandler::class);

        $handler->report($e);

        return $handler->render($passable, $e);
    }
}
