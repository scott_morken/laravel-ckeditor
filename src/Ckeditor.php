<?php
/**
 * Created by IntelliJ IDEA.
 * User: smorken
 * Date: 4/4/14
 * Time: 6:28 PM
 */

namespace Smorken\Ckeditor;

use Illuminate\Contracts\Routing\UrlGenerator;
use Illuminate\Support\Collection;
use Illuminate\View\Factory;
use Smorken\Html\FormBuilder;

class Ckeditor implements \Smorken\Ckeditor\Contracts\Ckeditor
{

    protected $config;
    protected $registered = [];
    protected $editor_opts = [];

    /**
     * Illuminate view environment.
     *
     * @var \Illuminate\View\Factory
     */
    protected $view;

    /**
     * @var FormBuilder
     */
    protected $form;

    /**
     * @var UrlGenerator
     */
    protected $url;

    public function __construct(Factory $view, FormBuilder $form, UrlGenerator $url, $config)
    {
        $this->view = $view;
        $this->form = $form;
        $this->url = $url;
        $this->config = $config;
    }

    public function create($name, $value = null, array $options = [], array $editor_opts = [])
    {
        $id = $this->form->getIdAttribute($name, $options);
        if ($id === null) {
            $id = $this->generateId($name);
            $options['id'] = $id;
        }
        $txt = $this->form->textarea($name, $value, $options);
        $this->register($id);
        $this->addEditorOptions($id, $editor_opts);
        return $txt;
    }

    public function generateId($name)
    {
        $count = count($this->registered());
        return $name . '-' . $count;
    }

    public function registered()
    {
        return $this->registered;
    }

    public function register($id)
    {
        $this->registered[] = $id;
    }

    public function addEditorOptions($id, array $editor_options = [])
    {
        if (!$this->editor_opts) {
            $this->editor_opts = new Collection();
        }
        if ($editor_options) {
            $default = array_get($this->config, 'options', []);
            $this->editor_opts->put($id, array_merge($default, $editor_options));
        }
    }

    /**
     * @param $request
     * @param \Symfony\Component\HttpFoundation\Response $response
     * Borrowed from Barryvdh/LaravelDebugBar
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function modifyResponse($request, $response)
    {
        $js = $this->generateJavascript();
        if ($js) {
            $content = $response->getContent();

            $pos = mb_strripos($content, '</body>');
            if (false !== $pos) {
                $content = mb_substr($content, 0, $pos) . $js . mb_substr($content, $pos);
            } else {
                $content = $content . $js;
            }
            $response->setContent($content);
        }
        return $response;
    }

    public function generateJavascript()
    {
        if ($this->isRegistered()) {
            return $this->view->make(
                'smorken/ckeditor::ckeditor',
                [
                    'source'     => $this->source(),
                    'registered' => $this->registered(),
                    'options'    => $this->options(),
                ]
            );
        }
    }

    public function isRegistered()
    {
        return count($this->registered) > 0;
    }

    public function options()
    {
        return $this->editor_opts;
    }

    public function source()
    {
        $src = array_get($this->config, 'source', 'js/ckeditor/ckeditor.js');
        if (strpos($src, '//') === false) {
            $src = $this->url->asset($src);
        }
        return $src;
    }
}
