<?php
/**
 * Created by IntelliJ IDEA.
 * User: smorken
 * Date: 4/6/14
 * Time: 12:34 PM
 */

use Mockery as m;

class CkeditorIntegrationTest extends AppTestCase
{

    /**
     * @var \Smorken\Ckeditor\Ckeditor
     */
    protected $sut;

    public function setUp()
    {
        parent::setUp();
        $this->sut = app('Smorken\Ckeditor\Contracts\Ckeditor');
    }

    public function testCreateSetsIsRegistered()
    {
        $txt = $this->sut->create('foo', 'bar', []);
        $expected = '<textarea id="foo-0" name="foo" cols="50" rows="10">bar</textarea>';
        $this->assertEquals($expected, $txt);
        $this->assertTrue($this->sut->isRegistered());
    }

    public function testGenerateJavascript()
    {
        $txt = $this->sut->create('foo', 'bar', []);
        $out = $this->sut->generateJavascript();
        $expected = '<script type="text/javascript" src="http://localhost/js/ckeditor/ckeditor.js"></script>
    <script type="text/javascript" id="ckeditor-foo-0">
    var ckeditoreditfoo0 = CKEDITOR.replace(\'foo-0\');
</script>
';
        $this->assertEquals($expected, (string)$out);
    }

    public function testGenerateJavascriptWithOptions()
    {
        $txt = $this->sut->create('foo', 'bar', [], ['foo' => 'bar']);
        $out = $this->sut->generateJavascript();
        $expected = '<script type="text/javascript" src="http://localhost/js/ckeditor/ckeditor.js"></script>
    <script type="text/javascript" id="ckeditor-foo-0">
    var ckeditoreditfoo0 = CKEDITOR.replace(\'foo-0\', {"foo":"bar"});
</script>
';
        $this->assertEquals($expected, (string)$out);
    }
}
