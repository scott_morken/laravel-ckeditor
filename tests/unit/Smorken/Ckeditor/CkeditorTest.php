<?php
/**
 * Created by IntelliJ IDEA.
 * User: smorken
 * Date: 4/6/14
 * Time: 12:34 PM
 */
use Mockery as m;
use Smorken\Ckeditor\Ckeditor;

class CkeditorTest extends \PackageTestCase
{

    /**
     * @var \Smorken\Ckeditor\Ckeditor
     */
    protected $sut;

    public function setUp()
    {
        parent::setUp();
        $this->view = m::mock('\Illuminate\View\Factory');
        $this->form = m::mock('\Smorken\Html\FormBuilder');
        $this->url = m::mock('Illuminate\Contracts\Routing\UrlGenerator');
        $this->url->shouldReceive('asset')->andReturnUsing(
            function ($path) {
                return 'public ' . $path;
            }
        );
        $this->sut = new Ckeditor($this->view, $this->form, $this->url, $this->getConfig());
    }

    public function tearDown()
    {
        m::close();
    }

    public function testDefaultIsNotRegistered()
    {
        $this->assertFalse($this->sut->isRegistered());
    }

    public function testRegisterSetsIsRegistered()
    {
        $this->sut->register(1);
        $this->assertTrue($this->sut->isRegistered());
    }

    public function testCreateSetsIsRegistered()
    {
        $this->form->shouldReceive('textarea')
                   ->once()
                   ->with('foo', 'bar', [])
                   ->andReturn('textarea html');
        $this->form->shouldReceive('getIdAttribute')
                   ->once()
                   ->with('foo', [])
                   ->andReturn('foo-id');
        $txt = $this->sut->create('foo', 'bar', []);
        $this->assertEquals('textarea html', $txt);
        $this->assertTrue($this->sut->isRegistered());
    }

    public function testCreateCanGenerateId()
    {
        $this->form->shouldReceive('textarea')
                   ->once()
                   ->with('foo', 'bar', ['id' => 'foo-0'])
                   ->andReturn('textarea html');
        $this->form->shouldReceive('getIdAttribute')
                   ->once()
                   ->with('foo', [])
                   ->andReturnNull();
        $txt = $this->sut->create('foo', 'bar', []);
        $this->assertEquals('textarea html', $txt);
        $this->assertTrue($this->sut->isRegistered());
        $this->assertEquals('foo-0', $this->sut->registered()[0]);
    }

    public function testGenerateJavascriptWithNoRegistersReturnsNull()
    {
        $this->assertNull($this->sut->generateJavascript());
    }

    public function testGenerateJavascript()
    {
        $this->view->shouldReceive('make')
                   ->once()
                   ->with(
                       'smorken/ckeditor::ckeditor',
                       [
                           'source'     => 'public foo/ckeditor.js',
                           'registered' => ['id-1'],
                           'options'    => [],
                       ]
                   )
                   ->andReturn('generated javascript');
        $this->sut->register('id-1');
        $out = $this->sut->generateJavascript();
        $this->assertEquals('generated javascript', $out);
    }

    public function testSourceDoesntModifyWithTwoSlashes()
    {
        $path = 'https://foo/bar/ckeditor.js';
        $sut = new Ckeditor(
            $this->view,
            $this->form,
            $this->url,
            [
                'source'  => $path,
                'options' => [],
            ]
        );
        $this->assertEquals($path, $sut->source());
    }

    public function testAddEditorOptionsBuildsCollection()
    {
        $this->sut->addEditorOptions('foo', ['opt' => 1]);
        $this->assertEquals(['opt' => 1], $this->sut->options()->get('foo'));
    }

    public function testAddEditorOptionsMergesWithDefaults()
    {
        $path = 'https://foo/bar/ckeditor.js';
        $sut = new Ckeditor(
            $this->view,
            $this->form,
            $this->url,
            [
                'source'  => $path,
                'options' => [
                    'biz' => 'baz',
                    'bar' => 1,
                ],
            ]
        );
        $sut->addEditorOptions('foo', ['bar' => 2]);
        $this->assertEquals(['biz' => 'baz', 'bar' => 2], $sut->options()->get('foo'));
    }

    public function testModifyResponseWithoutBodyTag()
    {
        $this->view->shouldReceive('make')
                   ->once()
                   ->with(
                       'smorken/ckeditor::ckeditor',
                       [
                           'source'     => 'public foo/ckeditor.js',
                           'registered' => ['id-1'],
                           'options'    => [],
                       ]
                   )
                   ->andReturn('generated javascript');
        $this->sut->register('id-1');
        $response = m::mock('Symfony\Component\HttpFoundation\Response');
        $response->shouldReceive('getContent')->once()->andReturn('foo content');
        $response->shouldReceive('setContent')->once()->with('foo contentgenerated javascript');
        $this->sut->modifyResponse(null, $response);
    }

    public function testModifyResponseWithBodyTag()
    {
        $this->view->shouldReceive('make')
                   ->once()
                   ->with(
                       'smorken/ckeditor::ckeditor',
                       [
                           'source'     => 'public foo/ckeditor.js',
                           'registered' => ['id-1'],
                           'options'    => [],
                       ]
                   )
                   ->andReturn('<script></script>');
        $this->sut->register('id-1');
        $response = m::mock('Illuminate\Http\Response');
        $response->shouldReceive('getContent')->once()->andReturn('<html><body>Foo</body></html>');
        $response->shouldReceive('setContent')->once()->with('<html><body>Foo<script></script></body></html>');
        $this->sut->modifyResponse(null, $response);
    }

    protected function getConfig()
    {
        return [
            'source'  => 'foo/ckeditor.js',
            'options' => [

            ],
        ];
    }
}
