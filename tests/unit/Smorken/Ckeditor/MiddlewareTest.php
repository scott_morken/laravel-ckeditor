<?php
/**
 * Created by IntelliJ IDEA.
 * User: scoce95461
 * Date: 6/20/16
 * Time: 12:03 PM
 */
use Mockery as m;

class MiddlewareTest extends \PackageTestCase
{

    public function tearDown()
    {
        m::close();
    }

    public function testMiddlewareReturnsResponse()
    {
        $c = m::mock('Smorken\Ckeditor\Ckeditor');
        $container = m::mock('Illuminate\Contracts\Container\Container');
        $response = m::mock('\Symfony\Component\HttpFoundation\Response');
        $request = m::mock('Illuminate\Http\Request');
        $sut = new \Smorken\Ckeditor\Middleware\Ckeditor($container, $c);
        $c->shouldReceive('modifyResponse')->with($request, $response)->once();
        $r = $sut->handle(
            $request,
            function ($request) use ($response) {
                return $response;
            }
        );
        $this->assertSame($response, $r);
    }

    public function testMiddlewareHandlesException()
    {
        $c = m::mock('Smorken\Ckeditor\Ckeditor');
        $container = m::mock('Illuminate\Contracts\Container\Container');
        $response = m::mock('\Symfony\Component\HttpFoundation\Response');
        $exc_response = m::mock('\Symfony\Component\HttpFoundation\Response');
        $request = m::mock('Illuminate\Http\Request');
        $handler = m::mock('Illuminate\Contracts\Debug\ExceptionHandler');
        $sut = new \Smorken\Ckeditor\Middleware\Ckeditor($container, $c);
        $c->shouldReceive('modifyResponse')->with($request, $exc_response)->once();
        $container->shouldReceive('bound')->once()->andReturn(true);
        $container->shouldReceive('make')->once()->with('Illuminate\Contracts\Debug\ExceptionHandler')->andReturn(
            $handler
        );
        $handler->shouldReceive('report')->once();
        $handler->shouldReceive('render')->once()->with($request, m::type('Exception'))->andReturn($exc_response);
        $r = $sut->handle(
            $request,
            function ($request) {
                throw new \Exception('Response fails');
            }
        );
        $this->assertSame($exc_response, $r);
    }
}
